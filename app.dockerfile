FROM php:7.3-fpm

RUN apt-get update && apt-get install -y libpq-dev libmcrypt-dev \
    mariadb-client libmagickwand-dev --no-install-recommends \
    && pecl install imagick \
    && pecl install mcrypt-1.0.2 \
    && docker-php-ext-enable imagick \
    && docker-php-ext-enable mcrypt \
    && docker-php-ext-install pgsql pdo pdo_pgsql pdo_mysql