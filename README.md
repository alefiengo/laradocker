# Entorno de desarrollo Laravel + Docker

La ventaja de tener un entorno de desarrollo es, principalmente, contar con una única configuración por cada uno de los desarrolladores del equipo de trabajo. Entonces, los nuevos desarrolladores no tienen que perder el tiempo en configurar el servidor web, las extensiones, dependencias, entre otros, sino dedicarse a desarrollar.

El objetivo de este proyecto no es aprender a programar en Laravel, tampoco aprender PostgreSQL ni MySQL, etc. 

El propósito es aprender a preparar un entorno de desarrollo con Docker. Para el ejemplo se tienen los siguientes componentes:

* Laravel, framework PHP para el desarrollo web.
* MySQL o PostgreSQL, para almacenar la información.
* Mailtrap, para gestión de correo electrónico.
* Redis, como almacén de datos en memoria, caché.

El proyecto está basado en la versión 6.* de Laravel, accesible desde su página oficial en GITHUB: *https://github.com/laravel/laravel/tree/6.x*.

## 0. Requerimientos Iniciales

Este proyecto se implementó en una distribución *Linux Ubuntu 18.04LTS*, con los siguientes paquetes instalados:

- [Docker 19.03.8](https://docs.docker.com/install)
- [Docker-Compose 1.25.4](https://docs.docker.com/compose/install)
- [Git 2.17.1](https://git-scm.com/downloads)

## 1. Clonar el proyecto

Como primer paso debemos clonar el proyecto, de la siguiente manera:

```
$ git clone https://gitlab.com/alefiengo/laradocker.git
```

A continuación ingresar en la carpeta principal del proyecto:

```
$ cd laradocker
```

Luego, preparar el archivo _**.env**_:

```
$ cp .env.example .env
```

Y configurarlo en base a las credenciales que se encuentran en el archivo _**docker-compose.yml**_.

## 2. Instalar dependencias

Instalaremos las dependencias vía Composer (Controlador de dependencias para PHP); esta tarea la realizaremos usando Docker. Para esto, debemos ejecutar el siguiente comando en una terminal:

```
$ sudo docker run --rm -it -v "$(pwd)":/app composer composer install
```

## 3. Levantar el proyecto

Estando seguros de que contamos con docker-compose instalado, levantamos el proyecto ejecutando el siguiente comando en una terminal:

```
$ sudo docker-compose up -d --build
```

Posteriormente, debemos realizar la migración de las tablas a la Base de Datos.

a) Ingresamos al contenedor _**app**_:

```
$ sudo docker-compose exec app bash
```

b) Dentro del contenedor ejecutamos las siguientes órdenes:

```
$ php artisan optimize:clear && php artisan migrate
```

c) Salir del contenedor

```
$ exit
```

Adicionalmente, debemos realizar las siguientes configuraciones:

a) En el archivo _**/etc/hosts**_:

```
$ sudo vim /etc/hosts
```

Agregar la siguiente línea:

```
127.0.0.1   laradocker.local    www.laradocker.local
```

b) Confgurar un administrador de base de datos (cliente) para postgres o mysql, en base a las credenciales que se encuentran en el archivo _**docker-compose.yml**_.

Y listo, ya tenemos nuestro entorno de desarrollo con Docker, accesible desde:

* Aplicación web: [http://laradocker.local:8080](http://laradocker.local:8080)

* Servidor de Correo: [http://localhost:8081](http://localhost:8081)
  - _**Usuario:**_    mailtrap
  - _**Contraseña:**_ mailtrap

## Autor

* **José Alejandro Fiengo Vega** - [alefiengo](https://gitlab.com/alefiengo)

## Fuente

La implementación del presente proyecto se basa en el Curso: **"Docker para entornos de desarrollo"**, disponible en [Udemy](https://www.udemy.com/share/102n4mAEYTdVdaR34=/).